/*
Nicholas Perez COSC 2336-302
Test 1 Part B - Modify
This class stores a persons's contact information; Their home phone number, their cellphone number, and their Email Address
*/

#ifndef H_CONTACT
#define H_CONTACT

#include <string>
 
using namespace std;

class contact{
private:
	string homePhone, cellPhone, emailAddress;

public:
	contact(string hPhone = "", string cPhone = "", string email = ""); // Constructor

	//Function declarations

	//Gets and Sets for homePhone
	void setHomePhone(string hPhone);
	string getHomePhone();
	
	//Gets and Sets for cellPhone
	void setCellPhone(string cPhone);
	string getCellPhone();
	
	//Gets and Sets for emailAddress
	void setEmailAddress(string email);
	string getEmailAddress();

	//Set all at once
	void setInfo(string hPhone, string cPhone, string email);
};



#endif