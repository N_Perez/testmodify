/*
Nicholas Perez COSC 2336-302
Test 1 Part B - Modify
*/
#include "contact.h"
#include <string>

using namespace std;


contact::contact(string hPhone, string cPhone, string email)
{
	homePhone = hPhone;
	cellPhone = cPhone;
	emailAddress = email;
}

void contact::setHomePhone(string hPhone)
{
	homePhone = hPhone;
}
string contact::getHomePhone()
{
	return homePhone;
}

void contact::setCellPhone(string cPhone)
{
	cellPhone = cPhone;
}
string contact::getCellPhone()
{
	return cellPhone;
}

void contact::setEmailAddress(string email)
{
	emailAddress = email;
}
string contact::getEmailAddress()
{
	return emailAddress;
}


void contact::setInfo(string hPhone, string cPhone, string email)
{
	homePhone = hPhone;
	cellPhone = cPhone;
	emailAddress = email;

}
