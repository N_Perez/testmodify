/*
Nicholas Perez COSC 2336-302
Test 1 Part B - Modify
*/

#include <iostream>
#include <string>
#include <iomanip>
#include "employee.h"

using namespace std;

employee::employee(string fName, string lName, string mName,
                int sDay, int sMnth, int sYear,
				string wrkPhn, int pay,
				string officeStreet, string officeTown, string officeState, string officeZip, string officeCounty, string officeCountry)
{	
}

void employee::print() 
{
    cout << "Employee: ";
    personType::print();
    cout << endl;

    cout << "Start date: ";
	startDate.printDate();
    cout << endl;
	
	cout << "Work Phone: " << workPhone << endl;

	cout << "Salary: $" << salary << endl;

	cout << "Office Address: ";
	officeAddress.print();
	cout << endl;

	cout << "Home Phone: " << contactInfo.getHomePhone() << endl;
	cout << "Cell Phone: " << contactInfo.getCellPhone() << endl;
	cout << "Email Address: " << contactInfo.getEmailAddress() << endl;
	
	cout << "Home Address: ";
	addressInfo.print();
	cout << endl;


}

void employee::setInfo(string fName, string lName, string mName,
                int sDay, int sMnth, int sYear,
				string wrkPhn, int pay,
				string officeStreet, string officeTown, string officeState, string officeZip, string officeCounty, string officeCountry)
{	
    setName(fName, lName, mName); // MODIFY added support for middle name
    startDate.setDate(sDay, sMnth, sYear);
	workPhone = wrkPhn;
	salary = pay;
	officeAddress.setInfo(officeStreet, officeTown, officeState, officeZip, officeCounty, officeCountry);

}

void employee::setStartDate(int sDay, int sMnth, int sYear)
{
	startDate.setDate(sDay, sMnth, sYear);
}
int employee::getStartDay()
{
	return startDate.getDay();
}

int employee::getStartMonth()
{
	return startDate.getMonth();
}
int employee::getStartYear()
{
	return startDate.getYear();
}

void employee::setWorkPhone(string wrkPhn)
{
	workPhone = wrkPhn;
}
string employee::getWorkPhone()
{
	return workPhone;
}


void employee::setSalary(int pay)
{
	salary = pay;
}
int employee::getSalary()
{
	return salary;
}

void employee::infoFromFile(string lineIn) //Takes a line from a comma seperated text file, passes it to extractor to break out a chunk, passes the chunk to fillField along with the field number to fill, then moves on to the next chunk
{	
	int fieldCurrent = 1; // field currently being filled
	int fieldsNeeded = 23; // Total number of fields that need to be filled with information
	int position = 0; // Position in string

	while (fieldCurrent <= fieldsNeeded) // Still got fields to fill
	{
		string temp = extractor(lineIn,position); //Toss it to extractor to get the information to fill the field being worked on
		fillField(temp,fieldCurrent); // Toss it to a helper to determine which field is being worked on, and fill it
		fieldCurrent++; //Move to the next field
	}
	
	return;

}
string employee::extractor(string lineIn, int& pos) //Breaks up the giant line into chunks seperated by commas
{	
	string temp = ""; //temp string to build the entry
	char c;
	while (pos < int(lineIn.length()))
	{
		if (lineIn.at(pos) == ',') // Found a comma, that's one field
		{
			pos++;
			return temp;
		}
		
		else
		{
		c = lineIn.at(pos); //Pulls a character from the string
		string str(1,c); // Converts the character to string
		temp.append(str); // Adds it to the word we're building
		pos++; // Move to next character
		}
	}
	return temp; //Reached the end

}
void employee::fillField(string info,int field)
{
	switch (field) { //This ugly monster of a switch determines which field is being worked on, and then uses the appropriate functions to fill said field.
		/*
			1: First Name
			2: Last Name
			3: Middle Name
			4: Starting Day
			5: Starting Month
			6: Starting Year
			7: Work Phone Number
			8: Salary
			9: Office Street address
			10: Office Town
			11: Office State
			12: Office Zip
			13: Office County
			14: Office Country
			15: Home Phone number
			16: Cell Phone number
			17: Email Address
			18:Home Street address
			19:Home Town
			20: Home State
			21: Home Zip
			23: Home County
			23: Home Country
		*/

	case 1: {
				setFirstName(info);
				break;
			}
	case 2:{
				setLastName(info);
				break;
		   }
	case 3: {
				setMiddleName(info);
				break;
			}
	case 4: {
				int day = stoi(info);
				startDate.setDay(day);
				break;
			}
	case 5:{
				int month = stoi(info);
				startDate.setMonth(month);
				break;
		   }
	case 6: {
				int year = stoi(info);
				startDate.setYear(year);
				break;
			}
	case 7: {
				setWorkPhone(info);
				break;
			}
	case 8:{
				int pay = stoi(info);
				setSalary(pay);
				break;
		   }
	case 9:{
				officeAddress.setStreet(info);
				break;
		   }
	case 10:{
				officeAddress.setTown(info);
				break;
			}
	case 11:{
				officeAddress.setState(info);
				break;
			}
	case 12:{
				officeAddress.setZip(info);
				break;
			}
	case 13:{
				officeAddress.setCounty(info);
				break;
			}
	case 14:{
				officeAddress.setCountry(info);
				break;
			}
	case 15:{
				contactInfo.setHomePhone(info);
				break;
			}
	case 16:{
				contactInfo.setCellPhone(info);
				break;
			}
	case 17:{
				contactInfo.setEmailAddress(info);
				break;
			}
	case 18:{
				addressInfo.setStreet(info);
				break;
			}
	case 19:{
				addressInfo.setTown(info);
				break;
			}
	case 20:{
				addressInfo.setState(info);
				break;
			}
	case 21:{
				addressInfo.setZip(info);
				break;
			}
	case 22:{
				addressInfo.setCounty(info);
				break;
			}
	case 23:{
				addressInfo.setCountry(info);
				break;
			}
	}
	return;
}