/*
Nicholas Perez COSC 2336-302
Test 1 Part B - Modify
*/
//Implementation file date
 
#include <iostream>
#include "dateType.h" 

using namespace std;

void dateType::setDate(int month, int day, int year)
{
    dMonth = month;
    dDay = day;
    dYear = year;
}

int dateType::getDay() const 
{
    return dDay;
}

int dateType::getMonth() const 
{
    return dMonth;
}

int dateType::getYear() const 
{
    return dYear;
}

void dateType::setDay(int day)
{
	dDay = day;
}

void dateType::setMonth(int month)
{
	dMonth = month;
}

void dateType::setYear(int year)
{
	dYear = year;
}

void dateType::printDate() const
{
    cout << dMonth << "-" << dDay << "-" << dYear;
}

    //Constructor with parameters
dateType::dateType(int month, int day, int year) 
{ 
    dMonth = month;
    dDay = day;
    dYear = year;
}

