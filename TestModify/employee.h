/*
Nicholas Perez COSC 2336-302
Test 1 Part B - Modify
This class stores information about Employees, including their name, the date they started, their work phone number, their salary, where their office is, their home address, and their contact information.
*/

#ifndef H_EMPLOYEE
#define H_EMPLOYEE

#include <string>
#include "personType.h"
#include "dateType.h"
 
using namespace std;

class employee: public personType
{
public:
    void print();

    void setInfo(string fName, string lName, string mName,
                int sDay, int sMnth, int sYear,
				string wrkPhn, int pay,
				string officeStreet, string officeTown, string officeState, string officeZip, string officeCounty, string officeCountry); 

    void setStartDate(int sDay, int sMnth, int sYear); //Sets the start date with Day, Month and Year
    int getStartDay(); // Gets the start day
    int getStartMonth(); // Gets the start month
    int getStartYear(); // Gets the start year
	
    void setWorkPhone(string wrkPhn); // Sets the work phone variable
    string getWorkPhone(); // Gets the word phone variage

	void setSalary(int pay); //Sets the salary
	int getSalary(); // Gets the salary 
	
    void infoFromFile(string lineIn); //Function builds employee records from a line pulled from a file

    employee(string fName = "", string lName = "", string mName = "",
                int sDay = 1, int sMnth = 1, int sYear = 1900,
				string wrkPhn = "", int pay = 0,
				string officeStreet = "", string officeTown = "", string officeState = "", string officeZip = "", string officeCounty = "", string officeCountry = ""); // Constructor
   
 private:
    dateType startDate;
	string workPhone;
	address officeAddress;
	int salary;

    string extractor(string lineIn, int& pos); //Helper function for file input
	void fillField(string info, int field); // Helper Function for file input
};

#endif
