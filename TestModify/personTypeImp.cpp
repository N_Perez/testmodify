//**********************************************************
// Author: D.S. Malik
// 
// Implementation file personTypeImp.cpp
// This file contains the definitions of the functions to 
// implement the operations of the class personType.
//**********************************************************
/*
Nicholas Perez COSC 2336-302
Test 1 Part B - Modify
*/

  
#include <iostream>
#include <string>
#include "personType.h"

using namespace std;

void personType::print() const
{
    cout << firstName << " " << middleName << " " << lastName; //MODIFY -- added middle name print
}

void personType::setName(string first, string last, string middle)
{
    firstName = first;
    lastName = last;
	middleName = middle; // MODIFY -- added middle name
}

string personType::getFirstName() const
{
    return firstName;
}

string personType::getLastName() const
{
    return lastName;
}

string personType::getMiddleName() const // MODIFY -- added function to get middle name
{
    return middleName;
}

void personType::setFirstName(string fName) 
{
    firstName = fName;
}

void personType::setLastName(string lName) 
{
    lastName = lName;
}

void personType::setMiddleName(string mName)  
{
    middleName = mName;
}
	//constructor
personType::personType(string first, string last, string middle) 
{ 
    firstName = first;
    lastName = last;
	middleName = middle; // MODIFY -- added middle name
}

