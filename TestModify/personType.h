//************************************************************
// Author: D.S. Malik
//  
// class personType
// This class specifies the members to implement a person's 
// first name and last name.
//************************************************************
 /*
Nicholas Perez COSC 2336-302
Test 1 Part B - Modify
*/

#ifndef H_personType
#define H_personType

#include <string>
#include "contact.h"
#include "address.h"

using namespace std;

class personType
{
public:
	contact contactInfo; //MODIFY -- Added contact info storage
	address addressInfo; // MODIFY -- added address info storage

    void print() const;
       //Function to output the first name and last name
       //in the form firstName lastName.
  
    void setName(string first, string last, string middle);
       //Function to set firstName and lastName //MODIFY -- and middleName //according  
       //to the parameters.
       //Postcondition: firstName = first; lastName = last //MODIFY middleName = middle

    string getFirstName() const;
       //Function to return the first name.
       //Postcondition: The value of the data member firstName
	   //               is returned.

    string getLastName() const;
       //Function to return the last name.
       //Postcondition: The value of the data member lastName
	   //               is returned.
	
    string getMiddleName() const; //MODIFY -- added function to get middle name
       //Function to return the middle name.
       //Postcondition: The value of the data member middleName
	   //               is returned.
	
    void setFirstName(string fName);
       //Function to set the first name.
       //Postcondition: The value of the data member firstName
	   //               is set.

    void setLastName(string lName);
       //Function to set the last name.
       //Postcondition: The value of the data member lastName
	   //               is set.
	
    void setMiddleName(string mName); //MODIFY -- added function to get middle name
       //Function to set the middle name.
       //Postcondition: The value of the data member middleName
	   //               is set.

    personType(string first = "", string last = "", string middle = "");
       //constructor
       //Sets firstName and lastName //MODIFY -- and middleName // according to the parameters.
       //The default values of the parameters are empty strings.
       //Postcondition: firstName = first; lastName = last // MODIFY middleName = middle

 private:
    string firstName; //variable to store the first name
    string lastName;  //variable to store the last name
	string middleName; //MODIFY -- variable to store the middle name

}; //Debug 01 -- missing ';'

#endif