/*
Nicholas Perez COSC 2336-302
Test 1 Part B - Modify
This class stores a persons's address with their Street, Town, State, Zipcode, County, and Country
*/

#ifndef H_ADDRESS
#define H_ADDRESS

#include <string>
 
using namespace std;


class address
{
private:
	string street, town, state, zip, county, country;

public:
	address(string strt = "", string twn = "", string stte = "", string zp = "", string cnty = "", string cntry = ""); // Constructor

	//Function declarations
	void print() const; //Prints address using a cout, Format: "[Street] [Town], [County] County, [State] [Zip] [Country]"

	//Gets and Sets for street
	void setStreet(string strt);
	string getStreet();
	
	//Gets and Sets for town
	void setTown(string twn);
	string getTown();
	
	//Gets and Sets for state
	void setState(string stte);
	string getState();
	
	//Gets and Sets for zip
	void setZip(string zp);
	string getZip();
	
	//Gets and Sets for county
	void setCounty(string cnty);
	string getCounty();
	
	//Gets and Sets for country
	void setCountry(string cntry);
	string getCountry();

	//Set all at once
	void setInfo(string strt, string twn, string stte, string zp, string cnty, string cntry); 
	
};


#endif
