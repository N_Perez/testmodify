/*
Nicholas Perez COSC 2336-302
Test 1 Part B - Modify
*/

#include <iostream>
#include "address.h"
#include <string>
#include <iomanip>

using namespace std;

address::address(string strt, string twn, string stte, string zp, string cnty, string cntry) // Constructor, sets all the variables to empty strings
{
	street = strt;
	town = twn;
	state = stte;
	zip = zp;
	county = cnty;
	country = cntry;
}

void address::print() const
{
	cout << street << endl << town << ", " << county << " County, " << state << " " << zip << " " << country ;
}


//gets and sets

void address:: setInfo(string strt, string twn, string stte, string zp, string cnty, string cntry) //Sets all fields with one function
{
	street = strt;
	town = twn;
	state = stte;
	zip = zp;
	county = cnty;
	country = cntry;
}
void address::setStreet(string strt)
{
	street = strt;
}
string address::getStreet()
{
	return street;
}

void address::setTown(string twn)
{
	town = twn;
}
string address::getTown()
{
	return town;
}

void address::setState(string stte)
{
	state = stte;
}
string address::getState()
{
	return state;
}

void address::setZip(string zp)
{
	zip = zp;
}
string address::getZip()
{
	return zip;
}

void address::setCounty(string cnty)
{
	county = cnty;
}
string address::getCounty()
{
	return county;
}

void address::setCountry(string cntry)
{
	country = cntry;
}
string address::getCountry()
{
	return country;
}
