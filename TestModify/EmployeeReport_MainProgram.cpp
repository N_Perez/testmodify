/*
Nicholas Perez COSC 2336-302
Test 1 Part B - Modify
*/

#include <iostream>
#include <string>
#include "personType.h"
#include "employee.h"
#include <fstream>

using namespace std;

void fileIn(string fileName, employee inArray[], int size); 
void printReport(employee inArray[], int size);

int main()
{
	const int MAX_SIZE = 5;
	employee employeeArray [MAX_SIZE];
	fileIn("EmployeeInfo.txt", employeeArray, MAX_SIZE);

    // Added Holdscreen for Command Line Compilers   
	system("pause");
	return 0;
}

void fileIn(string fileName, employee inArray[], int size) //Sets the fields for each employee in the array according to the information in the text file
{
	int i = 0;
	  string line;	  
	  ifstream myfile (fileName); //Open the word list
	  if (myfile.is_open()) 
	  {
		cout << "Loading employee records from file..." << endl;
		while ( getline (myfile,line) )
		{
			inArray[i].infoFromFile(line); //Passes a single line to the employee's infoFromFile function to set all the fields in it
			i++; // Move to the next employee in the array
		}
		myfile.close();
		printReport(inArray, size); 
	  }

	  else
	  {
		  cout << "Unable to open file.";
		  abort();
	  }
}


void printReport(employee inArray[], int size) //Prints each employee in the array's information
{	
		int i= 0;
		while (i<size)
		{
			cout << "Report #"  << i+1 << endl;
			inArray[i].print();
			cout << endl;
			i++;

		}
}